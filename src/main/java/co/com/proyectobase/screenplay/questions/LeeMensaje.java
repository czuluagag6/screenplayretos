package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.PaginaAutomationDemoSiteHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LeeMensaje implements Question<String>{

	public static LeeMensaje EnLaPaginaVerificacion() {
		return new LeeMensaje();
	}

	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(PaginaAutomationDemoSiteHomePage.TEXTO_CONFIRMACION).viewedBy(actor).asString();
	}

}


