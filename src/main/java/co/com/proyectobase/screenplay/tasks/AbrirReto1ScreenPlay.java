package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.PaginaAutomationDemoSiteHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirReto1ScreenPlay implements Task {

	private PaginaAutomationDemoSiteHomePage paginaAutomationDemoSiteHomePage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(paginaAutomationDemoSiteHomePage));
	}

	public static AbrirReto1ScreenPlay PaginaAutomationDemoSite() {
		return Tasks.instrumented(AbrirReto1ScreenPlay.class);
	}
	
}
