package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.model.Registro;
import co.com.proyectobase.screenplay.userinterface.PaginaAutomationDemoSiteHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class RegistroReto1ScreenPlay implements Task {

	private List<Registro> registro;
	
	public RegistroReto1ScreenPlay(List<Registro> registro) {
		super();
		this.registro = registro;
	}
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Enter.theValue(registro.get(0).getCampoNombre()).into(PaginaAutomationDemoSiteHomePage.CAMPO_NOMBRE));
		actor.attemptsTo(Enter.theValue(registro.get(0).getCampoApellido()).into(PaginaAutomationDemoSiteHomePage.CAMPO_APELLIDO));
		actor.attemptsTo(Enter.theValue(registro.get(0).getCampoDireccion()).into(PaginaAutomationDemoSiteHomePage.CAMPO_DIRECCION));
		actor.attemptsTo(Enter.theValue(registro.get(0).getEmail()).into(PaginaAutomationDemoSiteHomePage.CAMPO_EMAIL));
		actor.attemptsTo(Enter.theValue(registro.get(0).getCelular()).into(PaginaAutomationDemoSiteHomePage.CAMPO_CELULAR));
		actor.attemptsTo(Click.on(PaginaAutomationDemoSiteHomePage.GENERO));
		actor.attemptsTo(Click.on(PaginaAutomationDemoSiteHomePage.CRICKET));
		actor.attemptsTo(Click.on(PaginaAutomationDemoSiteHomePage.MOVIES));
		actor.attemptsTo(Click.on(PaginaAutomationDemoSiteHomePage.HOCKEY));
		actor.attemptsTo(Click.on(PaginaAutomationDemoSiteHomePage.LENGUAJE));
		actor.attemptsTo(SeleccionarLista.Desde(PaginaAutomationDemoSiteHomePage.SELECCIONAR_LENGUAJE, registro.get(0).getLenguaje().trim()));
		actor.attemptsTo(SelectFromOptions.byVisibleText(registro.get(0).getHabilidades()).from(PaginaAutomationDemoSiteHomePage.HABILIDADES));
		actor.attemptsTo(SelectFromOptions.byVisibleText(registro.get(0).getPais1()).from(PaginaAutomationDemoSiteHomePage.PAIS1));
		actor.attemptsTo(Click.on(PaginaAutomationDemoSiteHomePage.PAIS2));
		actor.attemptsTo(SeleccionarLista.Desde(PaginaAutomationDemoSiteHomePage.SELECCIONAR_PAIS2, registro.get(0).getPais2().trim()));
		actor.attemptsTo(SelectFromOptions.byVisibleText(registro.get(0).getYear()).from(PaginaAutomationDemoSiteHomePage.SELECCIONAR_YEAR));
		actor.attemptsTo(SelectFromOptions.byVisibleText(registro.get(0).getMes()).from(PaginaAutomationDemoSiteHomePage.SELECCIONA_MES));
		actor.attemptsTo(SelectFromOptions.byVisibleText(registro.get(0).getDia()).from(PaginaAutomationDemoSiteHomePage.SELECCIONAR_DIA));
		actor.attemptsTo(Enter.theValue(registro.get(0).getPassword()).into(PaginaAutomationDemoSiteHomePage.PASSWORD));
		actor.attemptsTo(Enter.theValue(registro.get(0).getConfirmarPassword()).into(PaginaAutomationDemoSiteHomePage.CONFIRMAR_PASSWORD));
		actor.attemptsTo(Click.on(PaginaAutomationDemoSiteHomePage.CLICK_BOTON_SUBMINT));
		try {
			Thread.sleep(5000);
		}
		catch(InterruptedException e) {
		}	
	}
	
	public static RegistroReto1ScreenPlay EnLaPaginaAutomationDemoSite(List<Registro> registro) {
		
		return Tasks.instrumented(RegistroReto1ScreenPlay.class, registro);
	}
	

}
