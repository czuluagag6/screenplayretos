package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://demo.automationtesting.in/Register.html")

public class PaginaAutomationDemoSiteHomePage extends PageObject{
	
	public static final Target CAMPO_NOMBRE = Target.the("CampoNombre").located(By.xpath("//*[@id='basicBootstrapForm']/div[1]/div[1]/input"));
	public static final Target CAMPO_APELLIDO = Target.the("CampoApellido").located(By.xpath("//*[@id='basicBootstrapForm']/div[1]/div[2]/input"));
	public static final Target CAMPO_DIRECCION = Target.the("CampoDireccion" ).located(By.xpath("//*[@id='basicBootstrapForm']/div[2]/div/textarea"));
	public static final Target CAMPO_EMAIL = Target.the("CampoEmail").located(By.xpath("//*[@id=\"eid\"]/input"));
	public static final Target CAMPO_CELULAR = Target.the("CampoCelular").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[4]/div/input"));
	public static final Target GENERO = Target.the("Genero").located(By.name("radiooptions"));
	public static final Target CRICKET = Target.the("Cricket").located(By.id("checkbox1"));
	public static final Target MOVIES =	Target.the("Movies").located(By.id("checkbox2"));
	public static final Target HOCKEY =	Target.the("Hockey").located(By.id("checkbox3"));
	public static final Target LENGUAJE = Target.the("CampoLenguaje").located(By.xpath("//*[@id=\"msdd\"]"));
	public static final Target SELECCIONAR_LENGUAJE = Target.the("SeleccionarLenguaje").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[7]/div/multi-select/div[2]/ul"));
	public static final Target HABILIDADES = Target.the("CampoHabilidades").located(By.id("Skills"));
	public static final Target PAIS1 = Target.the("Pais1").located(By.xpath("//*[@id=\"countries\"]"));
	public static final Target PAIS2 = Target.the("Pais2").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[10]/div/span/span[1]/span"));
	public static final Target SELECCIONAR_PAIS2 = Target.the("SeleccionarPais2").located(By.id("select2-country-results"));
	public static final Target SELECCIONAR_YEAR = Target.the("SeleccionarAño").located(By.xpath("//*[@id=\"yearbox\"]"));
	public static final Target SELECCIONA_MES = Target.the("SleccionarMes").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select"));
	public static final Target SELECCIONAR_DIA = Target.the("SeleccionarDia").located(By.xpath("//*[@id=\"daybox\"]"));
	public static final Target PASSWORD = Target.the("CampoPassword").located(By.xpath("//*[@id=\"firstpassword\"]"));
	public static final Target CONFIRMAR_PASSWORD = Target.the("CampoConfirmarPassword").located(By.xpath("//*[@id=\"secondpassword\"]"));
	public static final Target CLICK_BOTON_SUBMINT = Target.the("BotonSubmint").located(By.xpath("//*[@id=\"submitbtn\"]"));

	public static final Target TEXTO_CONFIRMACION = Target.the("TextoConfirmacion").located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));
}




