package co.com.proyectobase.screenplay.model;

public class Registro {

	private String campoNombre;
	private String campoApellido;
	private String campoDireccion;
	private String email;
	private String celular;
	private String password;
	private String confirmarPassword;
	private String lenguaje;
	private String habilidades;
	private String pais1;
	private String pais2;
	private String year;
	private String mes;
	private String dia;
	
	public String getCampoNombre() {
		return campoNombre;
	}
	
	public void setCampoNombre(String campoNombre) {
		this.campoNombre = campoNombre;
	}
	
	public String getCampoApellido() {
		return campoApellido;
	}
	
	public void setCampoApellido(String campoApellido) {
		this.campoApellido = campoApellido;
	}

	public String getCampoDireccion() {
		return campoDireccion;
	}

	public void setCampoDireccion(String campoDireccion) {
		this.campoDireccion = campoDireccion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmarPassword() {
		return confirmarPassword;
	}

	public void setConfirmarPassword(String confirmarPassword) {
		this.confirmarPassword = confirmarPassword;
	}

	public String getLenguaje() {
		return lenguaje;
	}

	public void setLenguaje(String lenguaje) {
		this.lenguaje = lenguaje;
	}

	public String getHabilidades() {
		return habilidades;
	}

	public void setHabilidades(String habilidades) {
		this.habilidades = habilidades;
	}

	public String getPais1() {
		return pais1;
	}

	public void setPais1(String pais1) {
		this.pais1 = pais1;
	}

	public String getPais2() {
		return pais2;
	}

	public void setPais2(String pais2) {
		this.pais2 = pais2;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	
	
	
}
