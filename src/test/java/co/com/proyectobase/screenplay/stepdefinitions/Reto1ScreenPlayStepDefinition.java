package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.Registro;
import co.com.proyectobase.screenplay.questions.LeeMensaje;
import co.com.proyectobase.screenplay.tasks.AbrirReto1ScreenPlay;
import co.com.proyectobase.screenplay.tasks.RegistroReto1ScreenPlay;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class Reto1ScreenPlayStepDefinition {
	
	@Managed (driver="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicialReto1(){
		
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^que Carlos quiere acceder a la Web Automation Demo Site$")
	public void queCarlosQuiereAccederALaWebAutomationDemoSite() throws Exception {
		
		carlos.wasAbleTo(AbrirReto1ScreenPlay.PaginaAutomationDemoSite());
	    
	}

	@When("^el realiza el registro en la página$")
	public void elRealizaElRegistroEnLaPágina(List<Registro> datosDemoSite) throws Exception {
		
		carlos.attemptsTo(RegistroReto1ScreenPlay.EnLaPaginaAutomationDemoSite(datosDemoSite));
	}

	@Then("^el verifica que se carga la pantalla con texto (.*)$")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow (String textoEsperado) throws Exception {
	    
		carlos.should(GivenWhenThen.seeThat(LeeMensaje.EnLaPaginaVerificacion(), Matchers.equalTo(textoEsperado)));
	}
}
